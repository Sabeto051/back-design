# Bancolombia Prueba Backend

## Setup

Renombra el archivo `./django_bancolombia/mysql.cnf.example` con el nuevo nombre `./django_bancolombia/mysql.cnf`, y cambia los campos para configurar tu conexión a la base de datos (MySql).


Install project dependencies:

```sh
$ pip install -r requirements.txt
```

Once `pip` has finished downloading the dependencies:
```sh
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```

## Apis

http://127.0.0.1:8000/api/clients/ [GET]
http://127.0.0.1:8000/api/clients/ [POST]
