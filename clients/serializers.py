from rest_framework import serializers
from clients.models import Client
from rest_framework.validators import UniqueValidator


class ClientSerializer(serializers.Serializer):
    """Client Serializer."""
    register_id = serializers.IntegerField(required=False)
    id = serializers.IntegerField()
    nro_prod = serializers.IntegerField()
    created = serializers.DateTimeField(required=False)


class CreateClientSerializer(serializers.Serializer):
    """Create Client Serializer."""
    register_id = serializers.IntegerField(
        required=False,
        validators=[
            UniqueValidator(queryset=Client.objects.all())
        ]
    )
    id = serializers.IntegerField()
    nro_prod = serializers.IntegerField()

    def create(self, validated_data):
        return Client.objects.create(**validated_data)
