import pandas as pd
import numpy as np
from clients.models import Client
from clients.serializers import CreateClientSerializer

# from clients.load_db_registers import fill_db


def fill_db():
    clients = pd.read_csv('./clients/assets/clientes.csv')
    clients.columns = ['id', 'nro_prod']

    for i in range(clients.shape[0]):
        Client.objects.create(
            id=clients.loc[i, 'id'],
            nro_prod=clients.loc[i, 'nro_prod'],
        )
    print('Clients Generated.')
