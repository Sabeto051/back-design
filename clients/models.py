from django.db import models

# Create your models here.


class Client(models.Model):
    """Client Model."""
    register_id = models.BigAutoField(primary_key=True)
    id = models.IntegerField()
    nro_prod = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return id."""
        return self.id
