from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from clients.models import Client
from clients.serializers import ClientSerializer, CreateClientSerializer

# Create your views here.


def save_client(data):
    """Save Client Model"""
    serializer = CreateClientSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    post = serializer.save()
    return post

@api_view(['GET', 'POST'])
def client_views(request):
    """Client Api."""

    if request.method == 'GET':
        """Get all clients."""
        clients = Client.objects.all()
        data = ClientSerializer(clients, many=True).data

        return Response(data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        """Create new Client."""
        if not isinstance(request.data, list):
            post = save_client(request.data)
        else:
            for client in request.data:
                post = save_client(client)
        return Response(ClientSerializer(post).data, status=status.HTTP_201_CREATED)
